\contentsline {section}{\numberline {1}Informacje og\'olne}{3}
\contentsline {section}{\numberline {2}Instrukcja obs\IeC {\l }ugi}{3}
\contentsline {section}{\numberline {3}Silnik gry, Rafa\IeC {\l } \.Zmijewski}{4}
\contentsline {subsection}{\numberline {3.1}Informacje og\'olne}{4}
\contentsline {subsection}{\numberline {3.2}Zarys klasy}{4}
\contentsline {subsection}{\numberline {3.3}Opis funkcji}{5}
\contentsline {subsection}{\numberline {3.4}Idee rozszerzenia klasy}{6}
\contentsline {section}{\numberline {4}Interfejs graficzny, Patryk Wojdy\IeC {\l }a}{6}
\contentsline {subsection}{\numberline {4.1}Informacje og\'olne}{6}
\contentsline {subsection}{\numberline {4.2}Og\'olny zarys klas}{6}
\contentsline {subsection}{\numberline {4.3}Om\'owienie funkcji}{7}
\contentsline {subsubsection}{\numberline {4.3.1}Funkcje klasy GameView}{7}
\contentsline {subsubsection}{\numberline {4.3.2}Funkcje klasy Area}{10}
\contentsline {subsection}{\numberline {4.4}Idee poszerzenia funkcjonalno\'sci}{11}
