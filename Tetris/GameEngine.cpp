#include "GameEngine.h"
#include <FL\Enumerations.H>
#include <iostream>
GameEngine :: GameEngine ( GameTable *table )
{
	gameTable=table;
}
bool GameEngine :: checkNearbyObstacle ( int move )
{
	switch ( move )
	{
	case FL_Left:
		for(int row=0;row<BOXHEIGHT;row++)
		{
			for (int column=0;column<BOXWIDTH;column++)
			{
				if(gameTable->boardTable[row][column]==BLOCK)
					if(gameTable->boardTable[row][column-1]==EDGE || gameTable->boardTable[row][column-1]==DROPEDBLOCK) return true;
			}
		}
		break;
	case FL_Right:
		for (int row=BOXHEIGHT-1;row>=0;row--)
		{
			for (int column=BOXWIDTH-1;column>=0;column--)
				if(gameTable->boardTable[row][column]==BLOCK)
					if(gameTable->boardTable[row][column+1]==EDGE || gameTable->boardTable[row][column+1]==DROPEDBLOCK) return true;
		}
		break;
	case FL_Down:
	case 32:
		for (int row=BOXHEIGHT-1;row>=0;row--)
		{
			for (int column=BOXWIDTH-1;column>=0;column--)
				if(gameTable->boardTable[row][column]==BLOCK)
					if(gameTable->boardTable[row+1][column]==EDGE || gameTable->boardTable[row+1][column]==DROPEDBLOCK) return true;
		}
		break;
	case FL_Up:
		if(gameTable->boardTable[gameTable->blocky+1][gameTable->blockx] ==EDGE || gameTable->boardTable[gameTable->blocky+1][gameTable->blockx] ==DROPEDBLOCK) return true;
		if(gameTable->boardTable[gameTable->blocky][gameTable->blockx+1] ==EDGE || gameTable->boardTable[gameTable->blocky][gameTable->blockx+1] ==DROPEDBLOCK) return true;
		if(gameTable->boardTable[gameTable->blocky][gameTable->blockx-1] ==EDGE || gameTable->boardTable[gameTable->blocky][gameTable->blockx-1] ==DROPEDBLOCK) return true;
		if(gameTable->boardTable[gameTable->blocky+1][gameTable->blockx-1] ==EDGE || gameTable->boardTable[gameTable->blocky+1][gameTable->blockx-1] ==DROPEDBLOCK) return true;
        if(gameTable->boardTable[gameTable->blocky+1][gameTable->blockx+1] ==EDGE || gameTable->boardTable[gameTable->blocky+1][gameTable->blockx+1] ==DROPEDBLOCK) return true;
		if((gameTable->boardTable[gameTable->blocky][gameTable->blockx+2] ==EDGE && gameTable->currentBlock==BLOCK) || (gameTable->boardTable[gameTable->blocky][gameTable->blockx+2] ==DROPEDBLOCK && gameTable->currentBlock==BLOCK)) return true;
		break;
	}
	return false;
}

void GameEngine :: checkRows ( void )
{
	int row,column,combo=0;
	for (row=BOXHEIGHT-2;row>=1;--row)
	{
		for (column=BOXWIDTH-1;column>=1;--column)
		{
			if(gameTable->boardTable[row][column]==0) break;
			if(column==1)
			{
				for (int x=row;x>=1;--x)
				{
					for (column=0;column<BOXWIDTH;++column)
					{
						if(gameTable->boardTable[x][column]!=EDGE)
						 gameTable->boardTable[x][column]=gameTable->boardTable[x-1][column];
					}
				}
				row=BOXHEIGHT-2;
				combo++;
			}
		}
	}
	gameTable->currentScore+=5*combo*combo;
}

void GameEngine :: makeAMove ( int move )
{
	int row,column;
	switch(move)
	{
		case FL_Left:
			if(checkNearbyObstacle(move))break;
			for(row=0;row<BOXHEIGHT;row++)
			{
				for(column=0;column<BOXWIDTH;column++)
				{
					if(gameTable->boardTable[row][column]==BLOCK)
					{
						gameTable->boardTable[row][column-1]=BLOCK;
						gameTable->boardTable[row][column]=0;
					}	
				}
			}
			gameTable->blockx-=1;
			break;
		case FL_Right:
			if(checkNearbyObstacle(move))break;
			for(row=BOXHEIGHT-1;row>=0;row--)
			{
				for(column=BOXWIDTH-1;column>=0;column--)
				{
					if(gameTable->boardTable[row][column]==BLOCK)
					{
						gameTable->boardTable[row][column+1]=BLOCK;
						gameTable->boardTable[row][column]=0;
					}	
				}
			}
			gameTable->blockx+=1;
			break;
		case FL_Down:
			if(checkNearbyObstacle(move))
			{
				for(row=BOXHEIGHT-1;row>=0;row--)
				{
					for(column=BOXWIDTH-1;column>=0;column--)
					{
						if(gameTable->boardTable[row][column]==BLOCK)
						{
							gameTable->boardTable[row][column]=DROPEDBLOCK;
						}	
					}
				}
				checkRows();
				if(!isGameOver())
				gameTable->createBlock();
			}
			else
			{
			for(row=BOXHEIGHT-1;row>=0;row--)
			{
				for(column=BOXWIDTH-1;column>=0;column--)
				{
					if(gameTable->boardTable[row][column]==BLOCK)
					{
						gameTable->boardTable[row+1][column]=BLOCK;
						gameTable->boardTable[row][column]=0;
					}	
				}
			}
			gameTable->blocky+=1;
			}
			break;
        case 32:
		case FL_Up:
			{
				if(checkNearbyObstacle(move))break;
				rotateBlock(gameTable->currentBlock,gameTable->state);
			}
	}
}

void GameEngine :: rotateBlock ( int currentBlock, int state )
{
	if(currentBlock==0)return;
	for ( int row=0;row<BOXHEIGHT;++row)
	{
		for ( int column=0;column<BOXWIDTH;++column)
		{
			if(gameTable->boardTable[row][column]==BLOCK)
				gameTable->boardTable[row][column]=0;
		}
	}
	switch ( currentBlock )
	{
	case 1:
		{
		switch (state)
		{
		case 0:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx+1]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx+2]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->state=1;
			gameTable->currentBlock=1;
			break;
		case 1:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-2][gameTable->blockx]=BLOCK;
			gameTable->state=0;
			gameTable->currentBlock=1;
			break;
		}
		break;
		}
	case 2:
		{
		switch (state)
		{
		case 0:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx+1]=BLOCK;
			gameTable->state=1;
			gameTable->currentBlock=2;
			break;
		case 1:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx-1]=BLOCK;
			gameTable->state=0;
			gameTable->currentBlock=2;
			break;
		}
		break;
		}
	case 3:
		{
		switch (state)
		{
		case 0:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx-1]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx+1]=BLOCK;
			gameTable->state=1;
			gameTable->currentBlock=3;
			break;
		case 1:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx-1]=BLOCK;
			gameTable->state=0;
			gameTable->currentBlock=3;
			break;
		}
		break;
		}
	case 4:
		{
		switch (state)
		{
		case 0:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx+1]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx-1]=BLOCK;
			gameTable->state=1;
			gameTable->currentBlock=4;
			break;
		case 1:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx-1]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx]=BLOCK;
			gameTable->state=2;
			gameTable->currentBlock=4;
			break;
		case 2:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx+1]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx+1]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->state=3;
			gameTable->currentBlock=4;
			break;
		case 3:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx+1]=BLOCK;
			gameTable->state=0;
			gameTable->currentBlock=4;
			break;
		}
		}
		break;
	case 5:
		{
		switch (state)
		{
		case 0:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx-1]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx+1]=BLOCK;
			gameTable->state=1;
			gameTable->currentBlock=5;
			break;
		case 1:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx+1]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx]=BLOCK;
			gameTable->state=2;
			gameTable->currentBlock=5;
			break;
		case 2:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx+1]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx+1]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->state=3;
			gameTable->currentBlock=5;
			break;
		case 3:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx-1]=BLOCK;
			gameTable->state=0;
			gameTable->currentBlock=5;
			break;
		}
		break;
		}
	case 6:
		{
		switch (state)
		{
		case 0:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->state=1;
			gameTable->currentBlock=6;
			break;
		case 1:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx+1]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->state=2;
			gameTable->currentBlock=6;
			break;
		case 2:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky-1][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx+1]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx]=BLOCK;
			gameTable->state=3;
			gameTable->currentBlock=6;
			break;
		case 3:
			gameTable->boardTable[gameTable->blocky][gameTable->blockx]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx+1]=BLOCK;
			gameTable->boardTable[gameTable->blocky][gameTable->blockx-1]=BLOCK;
			gameTable->boardTable[gameTable->blocky+1][gameTable->blockx]=BLOCK;
			gameTable->state=0;
			gameTable->currentBlock=6;
			break;
		}
		break;
		}
	}
}

void GameEngine :: newGame ( void )
{
	gameTable->reloadGameTable();
	gameTable->currentScore=0;
}

int GameEngine :: getCurrentScore ( void )
{
	return gameTable->currentScore;
}

bool GameEngine :: isGameOver ( void )
{
	if(gameTable->boardTable[3][5]==DROPEDBLOCK) return true;
	return false;
}

std::vector<std::string> GameEngine :: getRecords ( void )
{
	std::vector<std::string> vec;
	vec=gameTable->send_records();
	return vec;
}

void GameEngine :: setNewRecord ( const char* nick, int score )
{
    gameTable->addRecord(nick,score);
}