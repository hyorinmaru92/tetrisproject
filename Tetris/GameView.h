#pragma once
#define WIN32

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Draw.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_BMP_Image.H>
#include <FL/Fl_JPEG_Image.H>

#include <vector>
#include <string>

#include "Area.h"
#include "GameEngine.h"
#include "config.h"

class GameView : public Fl_Double_Window{
	
	public:
		GameView( GameTable *, GameEngine * );
		~GameView(void);
		friend void fullscreen_cb( Fl_Widget* w, void* );
		friend void window_cb( Fl_Widget* w, void* );
		void newGameActions( void );
		void printRecords();								// Z kontrolera otrzymywana bedzie tablica najlepszych wynikow
		void gameOver();
		void move( int );
		void pauseGame();
		friend void time(void*);

	private:
		GameEngine *kontroler;
		Fl_BMP_Image *logo;
		Fl_BMP_Image *biglogo;
		Fl_BMP_Image *startBitmap;
		Fl_BMP_Image *startbigBitmap;
		Fl_BMP_Image *stopBitmap;
		Fl_BMP_Image *stopbigBitmap;
		Fl_Menu_Bar *menu;									// Gorny pasek menu
		Fl_Output *current_score;							// Wyswietla biezacy wynik
		Fl_Box *stats;										// Pudelko, w ktorym beda znajdowac sie statystyki wraz z biezacym wynikiem
		Fl_Box *pause;
		Area *area;											// Pole gry
        int level;
        double delay;
		bool isPause;
		
};