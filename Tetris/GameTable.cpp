#include "GameTable.h"
#include <time.h>
#include <fstream>
GameTable :: GameTable (void)
{
	reloadGameTable();
	createBlock();
	currentScore=0;
}
		
void GameTable :: reloadGameTable ( void )
{
	for(int row=0; row<BOXHEIGHT; row++) //zerowanie talicy
		for(int column=0; column<BOXWIDTH; column++)
			boardTable[row][column]=0;

	for(int row=0; row<BOXHEIGHT; row++)//wypelnianie brzeg�w - zeby odroznic od klockow maja wart. -1
	{
		boardTable[row][0]=EDGE;
		boardTable[row][BOXWIDTH-1]=EDGE;
	}

	for(int column=0; column<BOXWIDTH; column++)
	{
		boardTable[BOXHEIGHT-1][column]=EDGE;
	}
	createBlock();
}

void GameTable :: createBlock ( void )
{
	int a=0;
	blocky=2;
	blockx=5;
	boardTable[blocky][blockx]=BLOCK;

	srand(time(NULL));
	a=rand()%19;
	switch(a)
		{	
			case 0:
				boardTable[blocky-1][blockx]=BLOCK;
				boardTable[blocky][blockx+1]=BLOCK;
				boardTable[blocky-1][blockx+1]=BLOCK;		// Kwadrat
				state=0;
				currentBlock=0;
				break;
			case 1:
				boardTable[blocky+1][blockx]=BLOCK;
				boardTable[blocky-1][blockx]=BLOCK;			// Prosty klocek 
				boardTable[blocky-2][blockx]=BLOCK;
				state=0;
				currentBlock=BLOCK;
				break;
			case 2:
				boardTable[blocky][blockx+1]=BLOCK;
				boardTable[blocky][blockx+2]=BLOCK;			// Prosty klocek obrot 90 st
				boardTable[blocky][blockx-1]=BLOCK;
				state=1;
				currentBlock=1;
				break;
			case 3:
				boardTable[blocky+1][blockx]=BLOCK;
				boardTable[blocky][blockx-1]=BLOCK;			// Klocek Z obrot 90 st
				boardTable[blocky-1][blockx-1]=BLOCK;
				state=0;
				currentBlock=2;
				break;
			case 4:
				boardTable[blocky][blockx-1]=BLOCK;
				boardTable[blocky-1][blockx]=BLOCK;			// Klocek Z 
				boardTable[blocky-1][blockx+1]=BLOCK;
				state=1;
				currentBlock=2;
				break;
			case 5:
				boardTable[blocky-1][blockx]=BLOCK;
				boardTable[blocky][blockx-1]=BLOCK;			// Klocek odwrocone Z 
				boardTable[blocky+1][blockx-1]=BLOCK;
				state=0;
				currentBlock=3;
				break;
			case 6:
				boardTable[blocky-1][blockx]=BLOCK;
				boardTable[blocky-1][blockx-1]=BLOCK;		// Klocek odwrocone z obrot 90 st
				boardTable[blocky][blockx+1]=BLOCK;
				state=1;
				currentBlock=3;
				break;
			case 7:
				boardTable[blocky-1][blockx]=BLOCK;
				boardTable[blocky+1][blockx]=BLOCK;			// Klocek L 
				boardTable[blocky+1][blockx+1]=BLOCK;
				state=0;
				currentBlock=4;
				break;
			case 8:
				boardTable[blocky][blockx+1]=BLOCK;
				boardTable[blocky][blockx-1]=BLOCK;			// Klocek L obrot 90 st
				boardTable[blocky+1][blockx-1]=BLOCK;
				state=1;
				currentBlock=4;
				break;
			case 9:
				boardTable[blocky-1][blockx]=BLOCK;
				boardTable[blocky-1][blockx-1]=BLOCK;		// Klocek L obrot o 180 st
				boardTable[blocky+1][blockx]=BLOCK;
				state=2;
				currentBlock=4;
				break;
			case 10:
				boardTable[blocky][blockx+1]=BLOCK;
				boardTable[blocky-1][blockx+1]=BLOCK;		// Klocek L obrot o 270 st	
				boardTable[blocky][blockx-1]=BLOCK;
				state=3;
				currentBlock=4;
				break;
			case 11:
				boardTable[blocky-1][blockx]=BLOCK;
				boardTable[blocky+1][blockx]=BLOCK;			// Klocek odwrocone L 
				boardTable[blocky+1][blockx-1]=BLOCK;
				state=0;
				currentBlock=5;
				break;
			case 12:
				boardTable[blocky-1][blockx-1]=BLOCK;
				boardTable[blocky][blockx-1]=BLOCK;			// Klocek odwrocone L obrot 90 st
				boardTable[blocky][blockx+1]=BLOCK;
				state=1;
				currentBlock=5;
				break;
			case 13:
				boardTable[blocky-1][blockx]=BLOCK;
				boardTable[blocky-1][blockx+1]=BLOCK;		// Klocek odwrocone L obrot 180 st
				boardTable[blocky+1][blockx]=BLOCK;
				state=2;
				currentBlock=5;
				break;
			case 14:
				boardTable[blocky+1][blockx+1]=BLOCK;
				boardTable[blocky][blockx+1]=BLOCK;			// Klocek odwrocone L obrot 270 st
				boardTable[blocky][blockx-1]=BLOCK;
				state=3;
				currentBlock=5;
				break;
			case 15:
				boardTable[blocky][blockx+1]=BLOCK;
				boardTable[blocky][blockx-1]=BLOCK;			// Klocek T
				boardTable[blocky+1][blockx]=BLOCK;
				state=0;
				currentBlock=6;
				break;			
			case 16:
				boardTable[blocky-1][blockx]=BLOCK;
				boardTable[blocky+1][blockx]=BLOCK;			// Klocek T obrot 90 st
				boardTable[blocky][blockx-1]=BLOCK;
				state=1;
				currentBlock=6;
				break;			
			case 17:
				boardTable[blocky-1][blockx]=BLOCK;
				boardTable[blocky][blockx+1]=BLOCK;			// Klocek T obrot 180 st
				boardTable[blocky][blockx-1]=BLOCK;
				state=2;
				currentBlock=6;
				break;			
			case 18:
				boardTable[blocky-1][blockx]=BLOCK;
				boardTable[blocky][blockx+1]=BLOCK;			// Klocek T obrot 270 st
				boardTable[blocky+1][blockx]=BLOCK;
				state=3;
				currentBlock=6;
				break;
		}
	
}

void GameTable :: loadRecords ( void )
{
	using namespace std;
	string name;
	string score;
	nameVec.clear();
	scoreVec.clear();
	ifstream records("best_score.bs");
	if(records.is_open())
	{
		while (!records.eof())
		{
			records>>name;
			decryptName(name);
			records>>score;
			nameVec.push_back(name);
			scoreVec.push_back(score);
		}
	}
	records.close();
}

void GameTable :: addRecord ( std::string name, int score)
{
	using namespace std;
	ofstream records("best_score.bs", ios::app);
	records<<endl;
	encryptScore(name);
	records<<name<<" "<<score;
	records.close();
	sort();
	records.close();
	send_records();
}

std::vector<std::string> GameTable::send_records()
{
	std::ostringstream o;
	std::vector<std::string> vec;
	loadRecords();
	std::string a;
	std::string b;
	int size=nameVec.size();
	if (size>5)
		size=5;
	for(int x=0; x<size; x++)
	{
		o.str("");
		a=nameVec[x];
		b=scoreVec[x];
		o<<x+1<<" "<<a<<" "<<b<<" ";
		vec.push_back(o.str().c_str());
	}
	return vec;
}

void GameTable :: sort()
{
	using namespace std;
	loadRecords ();
	for(int x=0; x<nameVec.size(); x++)
		for(int y=0; y<nameVec.size()-1-x; y++)
		{
			if(atoi(scoreVec[y].c_str())>atoi(scoreVec[y+1].c_str()))
			{
				swap(scoreVec[y],scoreVec[y+1]);
				swap(nameVec[y],nameVec[y+1]);
			}
		}
	ofstream records("best_score.bs");

	for(int x=nameVec.size()-1; x>=0; x--)
	{
        records<<endl;
		encryptScore(nameVec[x]);
		records<<nameVec[x]<<" ";
		records<<scoreVec[x];
	}
	records.close();
}

void GameTable :: encryptScore(std::string &name)
{
	int size=name.length();
	for(int x=0; x<size; x++)
		name[x]=name[x]+100;

}

void GameTable :: decryptName(std::string &name)
{
	int size=name.length();
	for(int x=0; x<size; x++)
	{
		name[x]=name[x]-100;
	}

}