#pragma once
// Og�lne parametry programu

// Kody element�w planszy 
#define  BIG_LOGO		   "gfx/biglogo.bmp"
#define	 LOGO			   "gfx/logo.bmp"
#define  LEFT_BORDER	   "gfx/left.bmp"
#define  RIGHT_BORDER	   "gfx/right.bmp"
#define  LEFT_BOTTOM_BORDER "gfx/leftbottom.bmp"
#define  RIGHT_BOTTOM_BORDER "gfx/rightbottom.bmp"
#define  BOTTOM_BORDER	   "gfx/bottom.bmp"
#define  BLUE_BLOCK		   "gfx/blue.bmp"
#define  RED_BLOCK		   "gfx/red.bmp"
#define  GREEN_BLOCK	   "gfx/green.bmp"
#define  YELLOW_BLOCK	   "gfx/yellow.bmp"
#define  STOP			   "gfx/stop.bmp"
#define  STOP_BIG		   "gfx/stopbig.bmp"
#define  START			   "gfx/start.bmp"
#define  START_BIG		   "gfx/startbig.bmp"

// Wymiary planszy
#define BOXHEIGHT 25
#define BOXWIDTH 12

// Warto�ci w tablicy

#define EDGE -1
#define BLOCK 1
#define DROPEDBLOCK -2
