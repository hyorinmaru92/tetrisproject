#pragma once

#define WIN32
#pragma once

#include "config.h"

#include <FL/Fl_Box.H>
#include <FL/Fl.H>
#include <FL/Fl_Draw.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_BMP_Image.H>


#include "GameTable.h"
#include "config.h"

class Area : public Fl_Box
{
	public:
		Area( int, int, GameTable* );
		~Area();

		void draw();
		int handle(int);
		

	private:
		Fl_BMP_Image *blueblockBitmap;
		Fl_BMP_Image *yellowblockBitmap;
		Fl_BMP_Image *redblockBitmap;
		Fl_BMP_Image *greenblockBitmap;
		
		Fl_BMP_Image *logoBMP;
		
		Fl_BMP_Image *leftborderBitmap;
		Fl_BMP_Image *rightborderBitmap;
		Fl_BMP_Image *rightbottomborderBitmap;
		Fl_BMP_Image *leftbottomborderBitmap;
		Fl_BMP_Image *bottomborderBitmap;
		
		GameTable *table;

        int randCache;
		
};