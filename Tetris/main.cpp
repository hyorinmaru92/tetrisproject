/*******************************
   Tetris
   Autorzy: 
	Patryk Wojdy�a
	Rafa� �mijewski
	Karol Wojtas
   Copyright 2013
********************************/

#include "GameTable.h"
#include "GameEngine.h"
#include "GameView.h"
#include <iostream>



int __stdcall WinMain(HINSTANCE a, HINSTANCE b, char* c, int d)
{
	GameTable table;
	GameEngine engine (&table);
	GameView view(&table,&engine);
	
	return Fl::run();
}