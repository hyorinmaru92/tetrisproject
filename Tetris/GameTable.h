#pragma once
#include "config.h"
#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>
class GameTable 
{
public:

	GameTable (void);
	void reloadGameTable ( void );
	void loadRecords ( void );
	void addRecord (std::string name, int score);
	void decryptName(std::string &name);
	void encryptScore(std::string &name);
	void sort();
	std::vector<std::string> send_records();
	std::vector<std::string> nameVec;
	std::vector<std::string> scoreVec;
	int boardTable [BOXHEIGHT][BOXWIDTH];
	void createBlock ( void );
	int blockx,blocky,state,currentBlock; // Pozycja srodka klocka, jego stan(obrot) oraz bierzaca wartosc
	int currentScore;
};