﻿#include "gameView.h"
#include <FL/fl_message.H>
#include <FL/Fl_Timer.H>
#include <sstream>
#include <string>

void fullscreen_cb( Fl_Widget* w, void* );
void window_cb( Fl_Widget* w, void* );
void info_cb( Fl_Widget*, void* );
void exit_cb( Fl_Widget* w, void* v );
void new_game_cb( Fl_Widget*, void* );
void records_cb( Fl_Widget*, void* );

Fl_Menu_Item menu_positions[] =
{
    { "Tetris", 0, 0, 0, FL_SUBMENU },
		{ "&New game", 0, new_game_cb  },
		{ "&Fullscreen", FL_CTRL+'f', fullscreen_cb  },
		{ "&Records", 0, records_cb  },
		{ "&Info", 0, info_cb },
		{ "&Window", FL_CTRL+'w', window_cb  },
		{ "&End", FL_CTRL+'e', exit_cb  },
    { 0 }
};

void fullscreen_cb( Fl_Widget* mn, void* )
{
	((Fl_Double_Window*)(mn->parent()))->border(1);
	((Fl_Double_Window*)(mn->parent()))->fullscreen();
	
	menu_positions->next(0)[2].hide();										// Ukrywanie opcji Fullscreen w menu górnym
	menu_positions->next(0)[5].show();										// Odsłonięcie opcji Window w menu górnym

	((GameView*)(mn->parent()))->menu->resize(0, 0, mn->parent()->w(), 30); // Rozciągnięcie paska menu górnego
	((GameView*)(mn->parent()))->menu->copy(menu_positions);				// Skopiowanie pozycji menu
	
	((GameView*)(mn->parent()))->area->resize( mn->parent()->w()/2-200, mn->parent()->h()/2-300, 360, 630); // Wyśrodkowanie obszaru gry

	
	((GameView*)(mn->parent()))->stats->resize( mn->parent()->w()- 510, 100, 500, 253);
	((GameView*)(mn->parent()))->stats->image(((GameView*)(mn->parent()))->biglogo);
	
	((GameView*)(mn->parent()))->current_score->resize( mn->parent()->w()- 330, mn->parent()->h()/2, 100, 30); // Zmiana pozycji wyniku bieżącego
	
	
	((GameView*)(mn->parent()))->stopBitmap = new Fl_BMP_Image(STOP_BIG);
	((GameView*)(mn->parent()))->startBitmap = new Fl_BMP_Image(START_BIG);
	((GameView*)(mn->parent()))->pause->image(((GameView*)(mn->parent()))->stopBitmap);
	((GameView*)(mn->parent()))->pause->resize( mn->parent()->w()-420, mn->parent()->h()-300, 300, 150 );

	mn->redraw();
}

void window_cb( Fl_Widget* mn, void* )
{
	((Fl_Double_Window*)(mn->parent()))->border(1);
	((Fl_Double_Window*)(mn->parent()))->fullscreen_off();
	
	menu_positions->next(0)[2].show();
	menu_positions->next(0)[5].hide();
	
	((GameView*)(mn->parent()))->menu->resize(0, 0, mn->parent()->w(), 30);
	((GameView*)(mn->parent()))->menu->copy(menu_positions);
	
	((GameView*)(mn->parent()))->stats->image(((GameView*)(mn->parent()))->logo);
	((GameView*)(mn->parent()))->stats->resize( mn->parent()->w()-320, mn->parent()->h()-600 , 300, 152);
	
	((GameView*)(mn->parent()))->area->resize(20, 40, 360, 630);
	((GameView*)(mn->parent()))->current_score->resize( mn->parent()->w()- 200, mn->parent()->h()- 400, 100, 30);

	((GameView*)(mn->parent()))->stopBitmap = new Fl_BMP_Image(STOP_BIG);
	((GameView*)(mn->parent()))->startBitmap = new Fl_BMP_Image(START_BIG);
	((GameView*)(mn->parent()))->pause->resize( mn->parent()->w()-270, mn->parent()->h()-280, 200, 100 );

	mn->redraw();
}

void GameView::move( int r )
{
	if(isPause==false) 
	{	
		std::ostringstream o;
		o << " " << kontroler->getCurrentScore();
		kontroler->makeAMove( r );
		current_score->value( o.str().c_str() );
		if( kontroler->isGameOver() )
		{
			gameOver();
			newGameActions();
		}
		area->redraw();
	}
}

void GameView::newGameActions( void )
{
	std::ostringstream o;
    Fl::remove_timeout(time,area);
    delay=0.7;
    level=1;
    o << " " << kontroler->getCurrentScore();
	kontroler->newGame();   // Przekazywanie do kontrolera
	current_score->value( o.str().c_str() );
    if(!isPause)
	    Fl::add_timeout(delay,time,area);
	redraw();
}

void new_game_cb( Fl_Widget* o, void* )
{
	((GameView*)((Fl_Menu_Bar *) o)->parent())->newGameActions();
}

void GameView::pauseGame()
{
	if(isPause==true)
	{
		isPause=false;
		pause->image(stopBitmap);
		Fl::add_timeout(delay,time,area);
	}
	else
	{
		pause->image(startBitmap);
		Fl::remove_timeout(time,area);
		isPause=true;
	}
	redraw();
}


void GameView::printRecords()
{
	std::ostringstream o;
	std::vector<std::string> vec;
	vec=kontroler->getRecords();
	Fl::remove_timeout(time,area);
	for(int i=0;i<vec.size();i++)
	{
		o<<"-------------------------------\n"<<vec.at(i)<<"\n";
	}
	o<<"-------------------------------\n";
	fl_message_title_default("Rekordy");
	fl_message( o.str().c_str());
	Fl::add_timeout(delay,time,area);
}

void records_cb( Fl_Widget* o, void* )
{
	((GameView*)((Fl_Menu_Bar *) o)->parent())->printRecords();
}


void info_cb( Fl_Widget*, void* )
{
   wchar_t t[] = L"Autorzy: \n \tPatryk Wojdyła \n \tRafał Żmijewski \n \tKarol Wojtas\n\n Copyright 2013";
   char s[100];
   fl_utf8fromwc(s, sizeof(s), t, sizeof(t));
   fl_message(s);
}

void exit_cb( Fl_Widget* w, void* v )
{
	w->parent()->hide();
}

void GameView::gameOver()
{
	const char* buf;
	Fl::remove_timeout(time,area);
	std::ostringstream os;
	os<<"Koniec gry! Uzyskany wynik to: "<<kontroler->getCurrentScore()<<" \n Wpisz swoje dane:";
	if(buf=fl_input(os.str().c_str())) 
	{
		if(buf=="") buf="Unknown";
		kontroler->setNewRecord(buf,kontroler->getCurrentScore());
	}
}


GameView::GameView( GameTable *plansza, GameEngine *k ) : Fl_Double_Window( 720, 690, "Tetris" )		// Konstruktor klasy GameView(), ktory inicjalizuje podwojne okno
{
	isPause=false;

	menu = new Fl_Menu_Bar(0, 0, w(), 30);							// Tworzenie paska menu
	menu->box(FL_PLASTIC_UP_BOX);									// Styl paska menu
	menu_positions->next(0)[5].hide();								// Ukrywanie opcji window ( powrot do mniejszego okna )
	menu->copy(menu_positions);										// Przypisanie elementow paska menu, ktore zostaly wczesniej zdefiniowane
	
	((Fl_Double_Window*)(menu->parent()))->color(FL_BLACK);			// Ustawienie koloru tla na czarny
	
	logo = new Fl_BMP_Image("gfx/logo.bmp");
	biglogo = new Fl_BMP_Image("gfx/biglogo.bmp");
	stats = new Fl_Box( w()-320, h()-600 , 300, 152);
	stats->image(logo);
	stats->box(FL_PLASTIC_DOWN_BOX);

	pause = new Fl_Box( w()-270, h()-280, 200, 100 );
	stopBitmap = new Fl_BMP_Image(STOP);
	stopbigBitmap = new Fl_BMP_Image(STOP_BIG);
	startBitmap = new Fl_BMP_Image(START);
	startbigBitmap = new Fl_BMP_Image(START_BIG);
	
	pause->image(stopBitmap);
	pause->box(FL_PLASTIC_DOWN_BOX);

	current_score = new Fl_Output( w()- 200, h()- 400, 100, 30, "Score:" );	// Stworzenie obiektu Output ( Sluzy do wyswietlania biezacego wyniku )
	current_score->value( " 0" );									// Wartosc domyslna 0
	current_score->box( FL_PLASTIC_DOWN_BOX );						// Styl pudelka
    current_score->visible_focus(0);								// Blokada kursora
	current_score->labelcolor(FL_WHITE);							// Kolor "score:"

	area = new Area( 20, 40, plansza );								// Tworzenie obszaru, w ktorym bedzie rozgrywana gra
	area->box(FL_PLASTIC_DOWN_BOX);									// Dodanie stylu do danego obszaru
	area->color(FL_RED);

	Fl::add_timeout(0.7,time,area);
	end();
	show();
	
	kontroler=k;
    level=1;
    delay=0.7;
}

void time(void *data)
{
	Fl_Box *o = (Fl_Box*)data;
    if((((GameView *) o->parent())->kontroler->getCurrentScore()%300 < 80) && (((GameView *) o->parent())->kontroler->getCurrentScore()>=300*((GameView *) o->parent())->level))
        {
            ((GameView *) o->parent())->level+=1;
            ((GameView *)o->parent())->delay-=0.1;
        }
	((GameView *) o->parent())->move( FL_Down );
    if(((GameView *)o->parent())->delay<=0.2)
        ((GameView *)o->parent())->delay=0.2;
	Fl::repeat_timeout(((GameView *)o->parent())->delay, time, data );
}

GameView::~GameView()
{
	delete menu;
	delete area;
	delete current_score;
	delete stats;
	delete logo;
	delete pause;
	delete startBitmap;
	delete startbigBitmap;
	delete stopBitmap;
	delete stopbigBitmap;
}