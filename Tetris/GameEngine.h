#pragma once
#include <vector>
#include <string>
#include "GameTable.h"

class GameEngine
{
public:

	GameEngine( GameTable* );

	void makeAMove ( int move );
	void newGame ( void );
	std::vector<std::string> getRecords ( void );
	bool checkNearbyObstacle ( int move );
	int getCurrentScore ( void );
	void checkRows ( void );
	void rotateBlock ( int currentBlock, int state );
	bool isGameOver ( void );
	void setNewRecord (const char* nick , int score);

private:
	GameTable *gameTable;
};