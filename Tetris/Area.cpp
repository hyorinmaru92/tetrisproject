#include "Area.h"
#include "GameView.h"
#include <time.h>
#include <cstdlib>

Area::Area( int x, int y, GameTable *pl ) : Fl_Box( x, y, 360, 630 )
{
	blueblockBitmap = new Fl_BMP_Image( BLUE_BLOCK );
	yellowblockBitmap = new Fl_BMP_Image( YELLOW_BLOCK );
	redblockBitmap = new Fl_BMP_Image( RED_BLOCK );
	greenblockBitmap = new Fl_BMP_Image( GREEN_BLOCK );

	logoBMP = new Fl_BMP_Image( LOGO );
	
	leftborderBitmap = new Fl_BMP_Image( LEFT_BORDER );
	rightborderBitmap = new Fl_BMP_Image( RIGHT_BORDER );
	rightbottomborderBitmap = new Fl_BMP_Image( RIGHT_BOTTOM_BORDER );
	leftbottomborderBitmap = new Fl_BMP_Image( LEFT_BOTTOM_BORDER );
	bottomborderBitmap = new Fl_BMP_Image( BOTTOM_BORDER );
	table=pl;
}

Area::~Area()
{
	delete blueblockBitmap;
	delete redblockBitmap;
	delete yellowblockBitmap;
	delete greenblockBitmap;

	delete logoBMP;
	
	delete leftborderBitmap;
	delete rightborderBitmap;
	delete rightbottomborderBitmap;
	delete leftbottomborderBitmap;
	delete bottomborderBitmap;
}

void Area::draw()
{
	int width_with_margin = x(), posX = 0;
    int height_with_margin = y(), posY = 0;
	int r=randCache;
	Fl_Box::draw();

	
    if(table->blocky==2)
    {
        while(true)
        {
            r=rand()%3;
            if(r!=randCache)
            {
                randCache=r;
                break;
            }
        }
    }

	for( int i=4; i<BOXHEIGHT; i++ )
      for( int j=0; j<BOXWIDTH; j++ )   
      { 
         posX = width_with_margin + j*30;
         posY = height_with_margin + (i-4)*30;
		 
		 if(j==0) leftborderBitmap->draw( posX, posY );
		 else if(j==(BOXWIDTH-1)) rightborderBitmap->draw( posX, posY );
		 else if(i==BOXHEIGHT-1) bottomborderBitmap->draw( posX, posY );
		 
		 if(j==0 && i==BOXHEIGHT-1) leftbottomborderBitmap->draw( posX, posY );
		 if(j==BOXWIDTH-1 && i==BOXHEIGHT-1) rightbottomborderBitmap->draw( posX, posY );
		 
		 switch(table->boardTable[i][j])
		 {
            case 1:
                switch(randCache)
				    {
					    case 0: greenblockBitmap->draw( posX, posY ); break;
					    case 1: yellowblockBitmap->draw( posX, posY ); break;
					    case 2: redblockBitmap->draw( posX, posY ); break;
					    default: greenblockBitmap->draw( posX, posY ); break;
				    }
			case -1: break;
            case -2: blueblockBitmap->draw( posX, posY ); break;
			default: break;
         }
		   
      }
}

 

int Area::handle(int e)									
{
	int r = Fl::event();
	if( r == FL_SHORTCUT )
	{
		int k = Fl::event_key();
		switch( k )
		{
			case FL_Up: case FL_Down: 
			case FL_Left: case FL_Right: 
			case 32:
				((GameView *) parent())->move( k ); break;
			case FL_Control_L: ((GameView *) parent())->pauseGame(); break; 
			default: return 0;
		}
		return 1;
	}
	return 0;
}

